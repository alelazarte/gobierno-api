package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.EntidadBean;
import ar.edu.ubp.das.db.Dao;

public class MSEntidadesDao extends Dao<EntidadBean, EntidadBean>{

	@Override
	public EntidadBean make(ResultSet result) throws SQLException {
		EntidadBean entidad = new EntidadBean();
		entidad.setId(result.getInt("id"));
		entidad.setNombre(result.getString("nombre"));
		entidad.setHabilitada(result.getBoolean("habilitada"));
		entidad.setEnCurso(result.getBoolean("en_curso"));
		return entidad;
	}

	@Override
	public EntidadBean insert(EntidadBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntidadBean update(EntidadBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntidadBean delete(EntidadBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EntidadBean> select(EntidadBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
    		this.connect();    		
    		this.setProcedure("dbo.get_entidades()");
    		return this.executeQuery();
    	}
    	finally {
            this.close();
    	}
	}

	@Override
	public boolean valid(EntidadBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	
	public int getIdServicioEntidad(int idEntidad) throws SQLException {
		try {
			this.connect();
			this.setStatement("select id from servicios where id_entidad = ?");
			this.setParameter(1, idEntidad);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				return result.getInt("id");
			}
			throw new SQLException("Entidad no encontrada");
		}
		finally {
			this.close();
		}
	}
	
	public int getIdServicioEntidadDeAsistencia(int idAsistencia) throws SQLException {
		try {
			this.connect();
			this.setStatement("select id from servicios where id_entidad = (select id_entidad from asistencias where id = ?)");
			this.setParameter(1, idAsistencia);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				return result.getInt("id");
			}
			throw new SQLException("Entidad no encontrada");
		}
		finally {
			this.close();
		}
	}
	
	public List<EntidadBean> selectEntidadesDisponibles(String cuil) throws SQLException {
		try {
    		this.connect();    		
    		this.setProcedure("dbo.get_entidades(?)");
    		this.setParameter(1, cuil);
    		return this.executeQuery();
    	}
    	finally {
            this.close();
    	}
	}

}
