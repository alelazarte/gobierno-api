package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.db.Dao;

public class MSUsuariosDao extends Dao<UsuarioBean, UsuarioBean> {

	@Override
	public UsuarioBean make(ResultSet result) throws SQLException {
		UsuarioBean bean = new UsuarioBean();
		bean.setCuil(result.getString("cuil"));
		bean.setNombre(result.getString("nombre"));
		bean.setApellido(result.getString("apellido"));
		bean.setEmail(result.getString("email"));
		bean.setHabilitado(result.getBoolean("habilitado"));
		bean.setVerificado(result.getBoolean("verificado"));
		return bean;
	}

	@Override
	public UsuarioBean insert(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
    		this.connect();    		
    		this.setProcedure("dbo.registrar_usuario(?, ?, ?, ?, ?, ?, ?)");
    		this.setParameter(1, bean.getCuil());
    		this.setParameter(2, bean.getNombre());
    		this.setParameter(3, bean.getApellido());
    		this.setParameter(4, bean.getEmail());
    		this.setParameter(5, true); // habilitado
    		this.setParameter(6, false); // verificado
    		this.setParameter(7, bean.getClave());
    		this.executeUpdate();
    		return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public UsuarioBean update(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioBean delete(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UsuarioBean> select(UsuarioBean bean) throws SQLException {
    	try {
    		this.connect();    		
    		this.setProcedure("dbo.get_usuario(?)");
    		this.setParameter(1, bean.getCuil());
    		return this.executeQuery();
    	}
    	finally {
            this.close();
    	}
	}

	@Override
	public boolean valid(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select hash_clave, verificado, habilitado from usuarios where cuil = ?");
			this.setParameter(1, bean.getCuil());
			ResultSet result = this.getStatement().executeQuery();
			if(! result.next()) {
				throw new SQLException("CUIL no registrado");
			}
    		if(! result.getBoolean("verificado")) {
    			throw new SQLException("Usuario no verificado");
    		}
    		if(! result.getBoolean("habilitado")) {
    			throw new SQLException("Usuario no habilitado");
    		}
    		if(! result.getString("hash_clave").contentEquals(bean.getClave())) {
    			throw new SQLException("Clave invalida");
    		}
    		return true;
		}
		finally {
			this.close();
		}
	}
	
	public int verificarUsuario(UsuarioBean bean) throws SQLException {
		try {
			this.connect();
			this.setProcedure("verificar_usuario(?)");
			this.setParameter(1, bean.getCuil());
			return this.executeUpdate();
		}
		finally {
			this.close();
		}
	}
	
}
