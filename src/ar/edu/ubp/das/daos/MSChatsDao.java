package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.db.Dao;

public class MSChatsDao extends Dao<MensajeBean, MensajeBean> {

	@Override
	public MensajeBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		MensajeBean bean = new MensajeBean();
		bean.setIdAsistencia(result.getInt("id_asistencia"));
		bean.setCliente(result.getString("cliente"));
		bean.setFecha(result.getDate("fecha"));
		bean.setId(result.getInt("id"));
		bean.setMensaje(result.getString("mensaje"));
		return bean;
	}

	@Override
	public MensajeBean insert(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		int consumido = 0;
		try {
			this.connect();
			this.setProcedure("agregar_mensaje(?,?,?,?)");
			this.setParameter(1, bean.getIdAsistencia());
			this.setParameter(2, bean.getCliente());
			this.setParameter(3, bean.getMensaje());
			if(bean.getCliente() == "entidad")
				consumido = 0;
			else
				consumido = 1;
			this.setParameter(4, consumido);
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public MensajeBean update(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MensajeBean delete(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MensajeBean> select(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		String procedure;

		if(bean.isNuevos())
			procedure = "get_mensajes_no_consumidos(?)";
		else
			procedure = "full_mensajes(?)";
		
		try {
			this.connect();
			this.setProcedure(procedure);
			this.setParameter(1, bean.getIdAsistencia());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public List<String> nuevosMensajesUsuario(int idAsistencia) throws SQLException {
		try {
			this.connect();
			this.setProcedure("nuevos_mensajes_usuario(?)");
			this.setParameter(1, idAsistencia);
			ResultSet result = this.getStatement().executeQuery();
			List<String> mensajes = new ArrayList<String>();
			while(result.next()) {
				mensajes.add(result.getString("mensaje"));
			}
			return mensajes;
		}
		finally {
			this.close();
		}
	}
}
