package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.DatoContactoBean;
import ar.edu.ubp.das.db.Dao;

public class MSDatosContactoDao extends Dao<DatoContactoBean, DatoContactoBean>{

	@Override
	public DatoContactoBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		DatoContactoBean bean = new DatoContactoBean();
		bean.setIdEntidad(result.getInt("id"));
		bean.setTipo(result.getString("tipo"));
		bean.setValor(result.getString("valor"));
		return bean;
	}

	@Override
	public DatoContactoBean insert(DatoContactoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatoContactoBean update(DatoContactoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatoContactoBean delete(DatoContactoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DatoContactoBean> select(DatoContactoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("datos_entidad(?)");
			this.setParameter(1, bean.getIdEntidad());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(DatoContactoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
