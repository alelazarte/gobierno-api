package ar.edu.ubp.das.daos;

import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;

import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.db.Dao;

public class MSSesionUsuariosDao extends Dao<UsuarioBean, UsuarioBean>{

	private String generarToken() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		return bytes.toString();
	}

	@Override
	public UsuarioBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		UsuarioBean sesion = new UsuarioBean();
		sesion.setTokenSesion(result.getString("token_sesion"));
		sesion.setCuil(result.getString("cuil"));
		sesion.setTokenVencimiento(result.getTime("token_vencimiento"));
		sesion.setNombre(result.getString("nombre"));
		return sesion;
	}

	@Override
	public UsuarioBean insert(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		// almacenar una nueva sesion
		try {
			String token = this.generarToken();
			bean.setTokenSesion(token);
			this.connect();
			this.setProcedure("registrar_token_usuario(?,?)");
			this.setParameter(1, bean.getCuil());
			this.setParameter(2, token);
			this.executeUpdate();

			this.setStatement("select * from usuarios u join sesion_usuario s on u.cuil = s.cuil where s.cuil = ?");
			this.setParameter(1, bean.getCuil());
			ResultSet result = this.getStatement().executeQuery();
			if(result.next())
				return this.make(result);
			return null;
		}
		finally {
			this.close();
		}
	}

	@Override
	public UsuarioBean update(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioBean delete(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("borrar_token(?)");
			this.setParameter(1, bean.getCuil());
			this.executeUpdate();
			return null;
		}
		finally {
			this.close();
		}
	}

	@Override
	public List<UsuarioBean> select(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean valid(UsuarioBean bean) throws SQLException, NullPointerException {
		// TODO Auto-generated method stub
		// comprobar token almacenado es el mismo que el bean
		// coincide con cuil
		// comprobar fechas correspondientes
		// comprobar usuario habilitado
		try {
			this.connect();    		
			this.setProcedure("dbo.obtener_token(?)");
			this.setParameter(1, bean.getCuil());
			List<UsuarioBean> sesiones = this.executeQuery();

			if (sesiones.isEmpty()) {
				return false;
			}
			if(!sesiones.iterator().hasNext()) {
				return false;
			}

			UsuarioBean sesion = sesiones.iterator().next();
			if (! bean.getCuil().contentEquals(sesion.getCuil())) {
				throw new SQLException("CUIL incorrecto");
			}
			if (! bean.getTokenSesion().contentEquals(sesion.getTokenSesion())) {
				throw new SQLException("Token invalido");
			}    		
			if(sesion.getTokenVencimiento().after(new Time(System.currentTimeMillis()))) {
				throw new SQLException("Token vencido");
			}
			this.setStatement("select habilitado from usuarios where cuil = ?");
			this.setParameter(1, bean.getCuil());
			ResultSet result = this.getStatement().executeQuery();
			if(! result.next()) {
				throw new SQLException("Error al comprobar usuario habilitado");
			}
			return result.getBoolean("habilitado");

		}
		finally {
			this.close();
		}
	}

}
