package ar.edu.ubp.das.daos;

import ar.edu.ubp.das.db.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.AsistenciaBean;

public class MSAsistenciasDao extends Dao<AsistenciaBean, AsistenciaBean> {

	@Override
	public AsistenciaBean make(ResultSet result) throws SQLException {
		AsistenciaBean bean = new AsistenciaBean();
		
		bean.setFechaSolicitada(result.getTimestamp("fecha_solicitada"));
		bean.setCuil(result.getString("cuil"));
		bean.setGeolocalizacion(result.getString("geolocalizacion"));
		bean.setId(result.getInt("id"));
		bean.setIdAsisEntidad(result.getInt("id_asis_entidad"));
		bean.setIdEntidad(result.getInt("id_entidad"));
		bean.setIdEstado(result.getInt("id_estado"));
		bean.setMensaje(result.getString("mensaje"));
		bean.setNombreEntidad(result.getString("nombre_entidad"));
		bean.setNombreEstado(result.getString("nombre_estado"));
		bean.setNombreUsuario(result.getString("nombre_usuario"));
		bean.setCancelada(result.getBoolean("cancelada"));
		bean.setFinalizada(result.getBoolean("finalizada"));
				
		return bean;
	}

	@Override
	public AsistenciaBean insert(AsistenciaBean bean) throws SQLException {
		try {
    		this.connect();    		
    		this.setProcedure("dbo.solicitar_asistencia(?, ?, ?, ?, ?)");
    		this.setParameter(1, bean.getCuil());
    		this.setParameter(2, bean.getIdEntidad());
    		this.setParameter(3, bean.getIdAsisEntidad());
    		this.setParameter(4, bean.getGeolocalizacion());
    		this.setParameter(5, bean.getMensaje());
    		ResultSet result = this.getStatement().executeQuery();
    		if(! result.next()) {
    			throw new SQLException("Error al traer id de asistencia creada");
    		}
    		int id = result.getInt("id");
    		AsistenciaBean asistencia = new AsistenciaBean();
    		asistencia.setId(id);
    		return asistencia;
		}
		finally {
			this.close();
		}
	}

	@Override
	public AsistenciaBean update(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("iniciar_asistencia(?)");
			this.setParameter(1, bean.getId());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public AsistenciaBean delete(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("cancelar_asistencia(?)");
			this.setParameter(1, bean.getId());
			this.executeUpdate();
			
    		this.setProcedure("is_habilitado_mes(?)");
    		this.setParameter(1, bean.getId());
    		this.executeUpdate();
    		
    		this.setProcedure("is_habilitado_seis_meses(?)");
    		this.setParameter(1, bean.getId());
    		this.executeUpdate();
    		
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public List<AsistenciaBean> select(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("historial_asistencias(?)");
			this.setParameter(1, bean.getCuil());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		
		// comprobar asistencia finalizada
		try {
			this.connect();
			this.setStatement("select id_estado from asistencias where id = ?");
			this.setParameter(1, bean.getId());
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				// 2 es estado "En proceso"
				return result.getInt("id_estado") > 2;
			}
			throw new SQLException("Error al consultar estado de asistencia");
		} finally {
			this.close();
		}
	}

	public AsistenciaBean get(int id) throws SQLException {
		try {
			this.connect();
			this.setProcedure("get_asistencia(?)");
			this.setParameter(1, id);
			List<AsistenciaBean> result = this.executeQuery();
			if (result.iterator().hasNext()) {
				return result.iterator().next();
			}
			return null;
		}
		finally {
			this.close();
		}
	}

	public void finalizarAsistencia(int idAsistencia) throws SQLException {
		try {
			this.connect();
			this.setProcedure("finalizar_asistencia(?)");
			this.setParameter(1, idAsistencia);
			this.executeUpdate();
		}
		finally {
			this.close();
		}
	}
	
	// en la plataforma necesito ver los datos de las asistencias en curso, como en la parte de historial
	// datos como la entidad, el mensaje, chats, detalles, etc.
	public List<AsistenciaBean> getAsistenciasEnCurso(String cuil) throws SQLException {
		try {
			this.connect();
			this.setProcedure("asistencias_en_curso(?)");
			this.setParameter(1, cuil);
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}
	
	public int[] asistenciasEnCursoIds(String cuil) throws SQLException {
		try {
			String statement = "select id from asistencias where finalizada = 0 and cancelada = 0";
			String idsString = "";
			
			this.connect();
			// get todas las asistencias, sin filtrar por cuil
			if(cuil != "*") {
				statement = statement + " and cuil = ?";
				this.setStatement(statement);
				// puede fallar por setear el parametro para el statement antes de setear el statement en si?
				this.setParameter(1, cuil);
			} else {
				this.setStatement(statement);
			}
			
			ResultSet result = this.getStatement().executeQuery();
			while(result.next())
				idsString = result.getInt("id") + " " + idsString;
				
			if(idsString == "")
				return new int[0];
			
			String[] exploded = idsString.split(" ");
			int[] ids = new int[exploded.length];
			for(int i=0; i<exploded.length; i++) {
				if(exploded[i] == "")
					break;
				System.out.println("exploded bit: "+exploded[i]);
				ids[i] = Integer.parseInt(exploded[i]);
			}
			
			return ids;
		}
		finally {
			this.close();
		}
	}
	
	// solo necesito saber si hay asistencias en curso
	public boolean comprobarAsistenciasEnCurso(String cuil) throws SQLException {
		try {
			this.connect();
			// deberia ser top 1, solo necesito saber si al menos hay una asistencia en curso
			this.setStatement("select id from asistencias where cuil = ? and finalizada = 0 and cancelada = 0");
			this.setParameter(1, cuil);
			ResultSet result = this.getStatement().executeQuery();
			return result.next();
		}
		finally {
			this.close();
		}
	}
	
}