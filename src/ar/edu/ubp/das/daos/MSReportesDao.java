package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ar.edu.ubp.das.db.Dao;

public class MSReportesDao extends Dao<String, String> {

	@Override
	public String make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String insert(String bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update(String bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete(String bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> select(String bean) throws SQLException {
		try {
			List<String> reporte = new LinkedList<String>();
			this.connect();
			reporte.add("REPORTE DE ENTIDADES\nentidad,finalizadas,canceladas,estado\n");
			this.setProcedure("generar_reporte_entidades");
			reporte.addAll(this.makeEntidad(this.getStatement().executeQuery()));
			reporte.add("REPORTE DE USUARIOS\ncuil,nombre,finalizadas,canceladas\n");
			this.setProcedure("generar_reporte_usuarios");
			reporte.addAll(this.makeUsuarios(this.getStatement().executeQuery()));
			return reporte;
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(String bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	
	private List<String> makeEntidad(ResultSet result) throws SQLException {
		List<String> rows = new ArrayList<String>();
		while(result.next()) {
			rows.add(String.format("%s,%d,%d,%s\n", 
				result.getString("entidad"),
				result.getInt("finalizadas"),
				result.getInt("canceladas"),
				result.getString("estado")));
		}
		return rows;
	}

	private List<String> makeUsuarios(ResultSet result) throws SQLException {
		List<String> rows = new ArrayList<String>();
		while(result.next()) {
			rows.add(String.format("%s,%s,%d,%d\n", 
				result.getString("cuil"),
				result.getString("nombre"),
				result.getInt("finalizadas"),
				result.getInt("canceladas")));
		}
		return rows;
	}
}
