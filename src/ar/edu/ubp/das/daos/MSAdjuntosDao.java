package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.AdjuntoBean;
import ar.edu.ubp.das.db.Dao;

public class MSAdjuntosDao extends Dao<AdjuntoBean, AdjuntoBean>{

	@Override
	public AdjuntoBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		AdjuntoBean bean = new AdjuntoBean();
		bean.setId(result.getInt("id"));
		bean.setIdAsistencia(result.getInt("id_asistencia"));
		bean.setPathPublico(result.getString("path_publico"));
		bean.setNombreArchivo(result.getString("nombre_archivo"));
		return bean;
	}

	@Override
	public AdjuntoBean insert(AdjuntoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("insert into adjunto_asistencia(id_asistencia, path_publico, nombre_archivo) values (?,?,?)");
			this.setParameter(1, bean.getIdAsistencia());
			this.setParameter(2, bean.getPathPublico());
			this.setParameter(3, bean.getNombreArchivo());
			this.executeUpdate();
		} finally {
			this.close();
		}
		return null;
	}

	@Override
	public AdjuntoBean update(AdjuntoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AdjuntoBean delete(AdjuntoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AdjuntoBean> select(AdjuntoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select * from adjunto_asistencia where id_asistencia = ?");
			this.setParameter(1, bean.getIdAsistencia());
			return this.executeQuery();
		} finally {
			this.close();
		}
	}

	@Override
	public boolean valid(AdjuntoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
}
