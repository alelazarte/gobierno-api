package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.CambioEstadoBean;
import ar.edu.ubp.das.db.Dao;

public class MSCambioEstadoDao extends Dao<CambioEstadoBean, CambioEstadoBean> {

	@Override
	public CambioEstadoBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		CambioEstadoBean bean = new CambioEstadoBean();
		bean.setFechaCambio(result.getTimestamp("fecha_cambio"));
		bean.setIdAsistencia(result.getInt("id_asistencia"));
		bean.setNombreEstado(result.getString("nombre_estado"));
		return bean;
	}

	@Override
	public CambioEstadoBean insert(CambioEstadoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CambioEstadoBean update(CambioEstadoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CambioEstadoBean delete(CambioEstadoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CambioEstadoBean> select(CambioEstadoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("get_cambio_estado_asistencia(?)");
			this.setParameter(1, bean.getIdAsistencia());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(CambioEstadoBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
