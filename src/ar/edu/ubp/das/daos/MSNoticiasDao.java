package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.NoticiaBean;
import ar.edu.ubp.das.db.Dao;

public class MSNoticiasDao extends Dao<NoticiaBean, NoticiaBean>{

	@Override
	public NoticiaBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		NoticiaBean noticia = new NoticiaBean();
		noticia.setAutor(result.getString("autor"));
		noticia.setTitulo(result.getString("titulo"));
		noticia.setCuerpo(result.getString("cuerpo"));
		noticia.setFechaPublicacion(result.getTimestamp("fecha_publicacion"));
		noticia.setFuente(result.getString("fuente"));
		return noticia;
	}

	@Override
	public NoticiaBean insert(NoticiaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("insert into noticias(titulo, autor, cuerpo, fuente, fecha_publicacion) values (?,?,?,?,sysdatetime())");
			this.setParameter(1, bean.getTitulo());
			this.setParameter(2, bean.getAutor());
			this.setParameter(3, bean.getCuerpo());
			this.setParameter(4, bean.getFuente());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public NoticiaBean update(NoticiaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NoticiaBean delete(NoticiaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NoticiaBean> select(NoticiaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("get_noticias()");
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(NoticiaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
