package ar.edu.ubp.das.daos;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import ar.edu.ubp.das.beans.SesionServicioBean;
import ar.edu.ubp.das.db.Dao;

public class MSSesionServiciosDao extends Dao<SesionServicioBean, SesionServicioBean> {

	@Override
	public SesionServicioBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		
		// TODO esto solo respeta la tabla de servicios, no la de sesion servicio, para
		// las funciones que usan la tabla sesion servicio deberian estar en un Dao aparte
		SesionServicioBean bean = new SesionServicioBean();
		bean.setClave(result.getString("clave"));
		bean.setUsuario(result.getString("usuario"));
		bean.setIdServicio(result.getInt("id"));
		bean.setBaseUrl(result.getString("base_url"));
		return bean;
	}

	@Override
	public SesionServicioBean insert(SesionServicioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("registrar_token_servicio(?,?)");
			this.setParameter(1, bean.getIdServicio());
			this.setParameter(2, bean.getToken());
			this.executeUpdate();
			System.out.println("token registrado?");
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public SesionServicioBean update(SesionServicioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SesionServicioBean delete(SesionServicioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SesionServicioBean> select(SesionServicioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select * from servicios where id = ?");
			this.setParameter(1, bean.getIdServicio());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(SesionServicioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select fecha_caducidad from sesion_servicio where id_servicio = ?");
			this.setParameter(1, bean.getIdServicio());
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				Date when = new Date(Calendar.getInstance().getTime().getTime());
				if(result.getDate("fecha_caducidad").before(when)) {
					return false;
				}
				return true;
			}
			return false;
		}
		finally {
			this.close();
		}
	}

	public String getToken(int idServicio) throws SQLException {
		try {
			this.connect();
			this.setStatement("select token, fecha_caducidad from sesion_servicio "
					+ "where id_servicio = ?");
			this.setParameter(1, idServicio);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				Date when = new Date(Calendar.getInstance().getTime().getTime());
				if(! result.getDate("fecha_caducidad").before(when)) {
					return result.getString("token");
				}
				return null;
			}
			return null;
		}
		finally {
			this.close();
		}
	}
	
	public SesionServicioBean getCredenciales(int idServicio) throws SQLException {
		try {
			this.connect();
			this.setStatement("select usuario, clave, base_url, ss.token as token from servicios s left join sesion_servicio ss on s.id = ss.id_servicio where s.id = ? ");
			this.setParameter(1, idServicio);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				SesionServicioBean credenciales = new SesionServicioBean();
				credenciales.setUsuario(result.getString("usuario"));
				credenciales.setClave(result.getString("clave"));
				credenciales.setBaseUrl(result.getString("base_url"));
				credenciales.setToken(result.getString("token"));
				return credenciales;
			}
			return null;
		}
		finally {
			this.close();
		}
	}
	
	public String getTecnologiaServicio(int idServicio) throws SQLException {
		try {
			this.connect();
			this.setStatement("select t.nombre as nombre from servicios s join tecnologias t on s.id_tecnologia = t.id where s.id = ?");
			this.setParameter(1, idServicio);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				return result.getString("nombre");
			}
			return null;
		}
		finally {
			this.close();
		}
	}
	
	public String getNombreEntidadServicio(int idServicio) throws SQLException {
		try {
			this.connect();
			this.setStatement("select e.nombre as nombre from servicios s join entidades e on s.id_entidad = e.id where s.id = ?");
			this.setParameter(1, idServicio);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				return result.getString("nombre");
			}
			return null;
		}
		finally {
			this.close();
		}
	}

}
