package ar.edu.ubp.das.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import ar.edu.ubp.das.beans.AdjuntoBean;
import ar.edu.ubp.das.beans.ArchivoBean;
import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.beans.CambioEstadoBean;
import ar.edu.ubp.das.beans.DatoContactoBean;
import ar.edu.ubp.das.beans.EntidadBean;
import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.beans.NoticiaBean;
import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.clientes.ClienteEmail;
import ar.edu.ubp.das.clientes.ClienteEntidad;
import ar.edu.ubp.das.daos.MSAdjuntosDao;
import ar.edu.ubp.das.daos.MSAsistenciasDao;
import ar.edu.ubp.das.daos.MSEntidadesDao;
import ar.edu.ubp.das.daos.MSUsuariosDao;
import ar.edu.ubp.das.db.Dao;
import ar.edu.ubp.das.db.DaoFactory;

@Path("/")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
// esto evita crear una nueva instancia por cada request
@Singleton
public class AsistenciasResource {

	private ClienteEntidad cliente;
	private String publicPath;
	private String endpointAdjuntos;
	private Algorithm algorithm;
	private JWTVerifier verifier;
	
	private HttpSession session;
	
	public AsistenciasResource() {
		this.cliente = new ClienteEntidad();
		this.publicPath = "/home/al/das/public";
		this.endpointAdjuntos = "http://localhost:8080/GobiernoAPI/api/asistencias/public/adjuntos";
		
		this.algorithm = Algorithm.HMAC256("replacethissecret");
		this.verifier = JWT.require(this.algorithm).build();
	}

	@GET
	@Path("/asistencias/{id}")
    public Response getAsistencia(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
    	try {
    		this.verifier.verify(auth);

    		MSAsistenciasDao dao = new MSAsistenciasDao();
    		AsistenciaBean asistencia = dao.get(id);
    		dao.close();
    		
    		System.out.println("getAsistencia("+id+") = "+asistencia.getId());
    		return Response.status(Response.Status.OK).entity(asistencia).build();
    	}
    	catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
    	catch(SQLException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
    	}
    }
	
	@GET
	@Path("/asistencias/isFinalizada/{id}")
	public Response isFinalizada(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
    		AsistenciaBean bean = new AsistenciaBean();
    		bean.setId(id);
    		
    		System.out.println("isFinalizada("+id+") = ...");
			return Response.status(Response.Status.OK).entity(dao.valid(bean)).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch (SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/enCurso/{cuil}")
	public Response enCurso(@HeaderParam("Authorization") String auth, @PathParam("cuil") String cuil) {
		try {
			this.verifier.verify(auth);
			MSAsistenciasDao dao = new MSAsistenciasDao();
			List<AsistenciaBean> asistencias = dao.getAsistenciasEnCurso(cuil);
			dao.close();
    		System.out.println("enCurso("+cuil+") = "+asistencias.size());
			return Response.status(Response.Status.OK).entity(asistencias).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch (SQLException e) {
			return Response.status(Response.Status.NO_CONTENT).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/comprobarEnCurso/{cuil}")
	public Response comprobarEnCurso(@HeaderParam("Authorization") String auth, @PathParam("cuil") String cuil) {
		try {
			this.verifier.verify(auth);
			MSAsistenciasDao dao = new MSAsistenciasDao();
			boolean enCurso = dao.comprobarAsistenciasEnCurso(cuil);
			dao.close();
			
    		System.out.println("comprobarEnCurso("+cuil+") = "+enCurso);
			return Response.status(Response.Status.OK).entity(enCurso).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch (SQLException e) {
			return Response.status(Response.Status.NO_CONTENT).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/{id}/adjuntos")
	public Response getAdjuntos(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			Dao<Void, AdjuntoBean> dao = DaoFactory.getDao("Adjuntos", "ar.edu.ubp.das");
			AdjuntoBean bean = new AdjuntoBean();
			bean.setIdAsistencia(id);
			
    		System.out.println("getAdjuntos("+id+") = ...");
			return Response.status(Response.Status.OK).entity(dao.select(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@POST
	@Path("/asistencias")
	public Response solicitar(@HeaderParam("Authorization") String auth, AsistenciaBean asistencia) {
		try {
			System.out.println("solicitar(): inicio");
			this.verifier.verify(auth);
			
			List<AdjuntoBean> adjuntos = new ArrayList<AdjuntoBean>();
			List<String> urls = new ArrayList<String>();
			String url = "";

			BufferedImage image;
			ByteArrayInputStream bis;
			MSAdjuntosDao daoAdjuntos = new MSAdjuntosDao();

			for(ArchivoBean a: asistencia.getArchivos()) {
				byte[] imageByte = Base64.getDecoder().decode(a.getContenido().split(",")[1]);

				bis = new ByteArrayInputStream(imageByte);
				image = ImageIO.read(bis);
				bis.close();

				if(image == null) {
					return Response.status(Response.Status.BAD_REQUEST).build();
				}

				// save the image locally
				ImageIO.write(image, "jpeg", new File("/home/al/das/public/" + a.getNombre()));

				url = this.endpointAdjuntos+"/"+a.getNombre();
				urls.add(url);

				System.out.println("guardar adjunto: "+a.getNombre()+", "+url);
				adjuntos.add(new AdjuntoBean(url, a.getNombre()));
			}

			asistencia.setUrlAdjuntos(String.join(", ", urls));

			int idAsisEntidad = this.cliente.solicitarAsistencia(asistencia);
			System.out.println("asistencia a entidad solicitada, id: "+idAsisEntidad);

			asistencia.setIdAsisEntidad(idAsisEntidad);
			Dao<AsistenciaBean, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			int idAsistencia = dao.insert(asistencia).getId();
			System.out.println("asistencia local creada, id: "+idAsistencia);


			for(AdjuntoBean a: adjuntos) {
				a.setIdAsistencia(idAsistencia);
				daoAdjuntos.insert(a);
			}	

			daoAdjuntos.close();

			System.out.println("solicitar(): fin");
			return Response.status(Response.Status.OK).entity(idAsistencia).build();

		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@POST
	@Path("/asistencias/iniciar/{id}")
	public Response iniciarAsistencia(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			AsistenciaBean bean = new AsistenciaBean();
			bean.setId(id);
			
			System.out.println("iniciarAsistencia("+id+") = ...");
			return Response.status(Response.Status.OK).entity(dao.update(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}


	@POST
	@Path("/asistencias/cancelar/{id}")
	public Response cancelarAsistencia(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			
			this.cliente.cancelarAsistencia(id);
			Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			AsistenciaBean bean = new AsistenciaBean();
			bean.setId(id);

			System.out.println("cancelarAsistencia("+id+") = ...");
			return Response.status(Response.Status.OK).entity(dao.delete(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch(Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/historial/{cuil}")
	public Response historial(@HeaderParam("Authorization") String auth, @PathParam("cuil") String cuil) {
		try {
			this.verifier.verify(auth);
			
			AsistenciaBean asistencia = new AsistenciaBean();
			asistencia.setCuil(cuil);
			Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.select(asistencia)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/chat/{id}")
	public Response historialChat(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			
			Dao<Void, MensajeBean> dao = DaoFactory.getDao("Chats", "ar.edu.ubp.das");
			MensajeBean bean = new MensajeBean();
			bean.setIdAsistencia(id);
			bean.setNuevos(false);
			return Response.status(Response.Status.OK).entity(dao.select(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/chatNuevos/{id}")
	public Response historialNuevos(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			this.verifier.verify(auth);
			
			Dao<Void, MensajeBean> dao = DaoFactory.getDao("Chats", "ar.edu.ubp.das");
			MensajeBean bean = new MensajeBean();
			bean.setIdAsistencia(id);
			bean.setNuevos(true);
			return Response.status(Response.Status.OK).entity(dao.select(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/asistencias/chat")
	public Response enviarMensaje(@HeaderParam("Authorization") String auth, MensajeBean bean) {
		try {
			this.verifier.verify(auth);
			
			Dao<Void, MensajeBean> dao = DaoFactory.getDao("Chats", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.insert(bean)).build();
		}
		catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	}
		catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/estados/{idAsistencia}")
	public Response cambioEstadoAsistencia(@HeaderParam("Authorization") String auth, @PathParam("idAsistencia") int idAsistencia) {
		try {
			this.verifier.verify(auth);
			
			Dao<Void, CambioEstadoBean> dao = DaoFactory.getDao("CambioEstado", "ar.edu.ubp.das");
			CambioEstadoBean bean = new CambioEstadoBean();
			bean.setIdAsistencia(idAsistencia);
			return Response.status(Response.Status.OK).entity(dao.select(bean)).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/asistencias/public/adjuntos/{nombre}")
	@Produces("image/jpeg")
	public Response getImagen(@PathParam("nombre") String nombre) {
		try {
			BufferedImage bImage = ImageIO.read(new File(this.publicPath+"/"+nombre));
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ImageIO.write(bImage, "jpeg", bos );
			byte [] data = bos.toByteArray();
			return Response.ok().entity(new StreamingOutput(){
				@Override
				public void write(OutputStream output)
					throws IOException, WebApplicationException {
					output.write(data);
					output.flush();
				}
			}).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();

		}
	}


	@GET
	@Path("/public/reportes/{nombre}")
	@Produces("application/text-plain")
	public InputStream getReporte(@PathParam("nombre") String nombre) {
		try {
			return new FileInputStream(this.publicPath +"/"+ nombre);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * NOTICIAS --------------------------------------------------------------------------------------------------------
	 */

	@GET
	@Path("/noticias")
	public Response noticias(@Context HttpServletRequest request, @HeaderParam("Authorization") String auth) {
		try {
			this.verifier.verify(auth);
						
			System.out.println("noticias()");
			Dao<Void, NoticiaBean> dao = DaoFactory.getDao("Noticias", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.select(null)).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	/*
	 * USUARIOS --------------------------------------------------------------------------------------------------------
	 */

	@PUT
	@Path("/usuarios")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registrar(UsuarioBean usuario) {
		try {
			ClienteEmail clienteEmail = new ClienteEmail();
			clienteEmail.enviarEmail(usuario.getEmail(), "RescueApp: verifique su email", "Bienvenido "+usuario.getNombre()+" "+usuario.getApellido()+" a la plataforma RescupeApp. Para completar su registro, por favor haga http://localhost:4200/sesion/verificar/"+usuario.getCuil());

			Dao<Void, UsuarioBean> dao = DaoFactory.getDao("Usuarios", "ar.edu.ubp.das");
			dao.insert(usuario);
			return Response.status(Response.Status.OK).build();
		} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@POST
	@Path("/usuarios")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest request, @HeaderParam("Authorization") String auth) {
		try {    		
			String[] credenciales = auth.split(":");
			UsuarioBean bean = new UsuarioBean();
			bean.setCuil(credenciales[0]);
			bean.setClave(credenciales[1]);

			Dao<Void, UsuarioBean> dao = DaoFactory.getDao("Usuarios", "ar.edu.ubp.das");
			if(! dao.valid(bean))
				throw new Exception("Login invalido");

			String token = JWT.create().withIssuer(bean.getCuil()).sign(this.algorithm);

			bean.setTokenSesion(token);
			
			this.session = request.getSession(true);
			this.session.setAttribute(token, bean);
			System.out.println(token);
			
			return Response.status(Response.Status.OK).entity(bean).build();
		} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		} catch(Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
		}
	}

	@GET
	@Path("/usuarios/verificar/{cuil}")
	public Response verificarUsuario(@PathParam("cuil") String cuil) {
		try {
			UsuarioBean usuario = new UsuarioBean();
			usuario.setCuil(cuil);
			MSUsuariosDao usuariosDao = new MSUsuariosDao();
			usuariosDao.verificarUsuario(usuario);
			usuariosDao.close();
			return Response.status(Response.Status.OK).build();
		} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/usuarios/{cuil}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response datosUsuario(@HeaderParam("Authorization") String auth, @PathParam("cuil") String cuil) {
		try {
			this.verifier.verify(auth);
			Dao<UsuarioBean, UsuarioBean> dao = DaoFactory.getDao("Usuarios", "ar.edu.ubp.das");
			UsuarioBean bean = new UsuarioBean();
			bean.setCuil(cuil);
			List<UsuarioBean> usuarios = dao.select(bean);
			if(usuarios.size() == 0)
				return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no existe").build();	
			return Response.status(Response.Status.OK).entity(usuarios.get(0)).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	/*
	 * ENTIDADES ---------------------------------------------------------------------------------------------------------
	 */
	@GET
	@Path("/entidades")
	public Response all(@HeaderParam("Authorization") String auth) {
		try {
			String cuil = this.verifier.verify(auth).getIssuer();

			System.out.println("cuil is "+cuil);
			MSEntidadesDao dao = new MSEntidadesDao();
			List<EntidadBean> entidades = dao.selectEntidadesDisponibles(cuil);
			dao.close();
			return Response.status(Response.Status.OK).entity(entidades).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

	@GET
	@Path("/entidades/{id}")
	public Response all(@HeaderParam("Authorization") String auth, @PathParam("id") int id) {
		try {
			Dao<Void, DatoContactoBean> dao = DaoFactory.getDao("DatosContacto", "ar.edu.ubp.das");
			DatoContactoBean bean = new DatoContactoBean();
			bean.setIdEntidad(id);
			return Response.status(Response.Status.OK).entity(dao.select(bean)).build();
		} catch(JWTDecodeException | SignatureVerificationException e) {
    		e.printStackTrace();
    		return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
    	} catch(SQLException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
}
