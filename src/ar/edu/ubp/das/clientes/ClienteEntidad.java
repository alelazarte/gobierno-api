package ar.edu.ubp.das.clientes;

import java.io.FileWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.beans.NoticiaBean;
import ar.edu.ubp.das.beans.SesionServicioBean;
import ar.edu.ubp.das.daos.MSAsistenciasDao;
import ar.edu.ubp.das.daos.MSEntidadesDao;
import ar.edu.ubp.das.daos.MSSesionServiciosDao;
import ar.edu.ubp.das.db.Dao;
import ar.edu.ubp.das.db.DaoFactory;

public class ClienteEntidad {
	
	private Dictionary<Integer,IClienteWS> clientesPool;
	
	public ClienteEntidad() {
		System.out.println("... por ende tambien un nuevo cliente entidad");
		this.clientesPool = new Hashtable<Integer, IClienteWS>();
	}
	
	private SesionServicioBean obtenerCredenciales(int idServicio) throws SQLException {
		MSSesionServiciosDao dao = new MSSesionServiciosDao();
		SesionServicioBean bean = dao.getCredenciales(idServicio);
		dao.close();
		return bean;
	}
	
	private <T> IClienteWS getCliente(int idServicio) throws SQLException, Exception {
		IClienteWS cliente = this.clientesPool.get(idServicio);
		if(cliente != null)
			return cliente;

		MSSesionServiciosDao dao = new MSSesionServiciosDao();
		String tecnologia = dao.getTecnologiaServicio(idServicio);
		SesionServicioBean credenciales = this.obtenerCredenciales(idServicio);
		IClienteWS clienteNuevo = null;
		dao.close();
		
		System.out.println("base url: "+credenciales.getBaseUrl());
		System.out.println("usuario: "+credenciales.getUsuario());
		System.out.println("clave: "+credenciales.getClave());
		
		switch(tecnologia) {
		case "REST":
			clienteNuevo = (IClienteWS) new ClienteRest(credenciales);
			this.clientesPool.put(idServicio, clienteNuevo);
			break;
		case "SOAP":
			clienteNuevo = (IClienteWS) new ClienteSOAP(credenciales);
			this.clientesPool.put(idServicio, clienteNuevo);
			break;
		}
		
		return clienteNuevo;
	}
	
	private int getIdAsistenciaEntidad(int id) throws SQLException {
		MSAsistenciasDao dao = new MSAsistenciasDao();
		int idAsisEntidad = dao.get(id).getIdAsisEntidad();
		dao.close();
		return idAsisEntidad;
	}

	private int getIdServicioEntidad(int idEntidad) throws SQLException {
		MSEntidadesDao dao = new MSEntidadesDao();
		int id = dao.getIdServicioEntidad(idEntidad);
		dao.close();
		return id;
	}
	// TODO borrar este y usar el de arriba
	private int getIdServicioEntidadDeAsistencia(int idAsistencia) throws SQLException {
		MSEntidadesDao dao = new MSEntidadesDao();
		int id = dao.getIdServicioEntidadDeAsistencia(idAsistencia);
		dao.close();
		return id;
	}

	
	private List<MensajeBean> getMensajesNuevosEntidad(int idAsistencia) throws SQLException, Exception {
		int idServicio = this.getIdServicioEntidadDeAsistencia(idAsistencia);
		IClienteWS cliente = this.getCliente(idServicio);
		
		MSAsistenciasDao dao = new MSAsistenciasDao();
		dao.close();

		int idAsistenciaEntidad = this.getIdAsistenciaEntidad(idAsistencia);
		System.out.println("a punto de conseguir nuevos mensajes para asis id: "+idAsistencia+", id asis entidad: "+idAsistenciaEntidad);
		return cliente.nuevosMensajesEntidad(idAsistenciaEntidad);
	}

	public void cargarNuevosMensajes(int idAsistencia) throws SQLException, Exception {
		
		if(this.isAsistenciaFinalizada(idAsistencia))
			return;
		
		Dao<Void, MensajeBean> dao = DaoFactory.getDao("Chats", "ar.edu.ubp.das");
		List<MensajeBean> mensajes = this.getMensajesNuevosEntidad(idAsistencia);
		
		if(mensajes == null)
			return;
		
		if(mensajes.size() > 0) {
			mensajes.forEach(m -> {
				try {
					MensajeBean bean = m;
					bean.setIdAsistencia(idAsistencia);
					bean.setCliente("entidad");
					dao.insert(bean);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		}
	}
	
	public boolean cancelarAsistencia(int id) throws Exception {
		int idServicio = this.getIdServicioEntidadDeAsistencia(id);
		IClienteWS cliente = this.getCliente(idServicio);

		return cliente.cancelarAsistencia(this.getIdAsistenciaEntidad(id));
	}
	
	public int solicitarAsistencia(AsistenciaBean bean) throws Exception {
		int idServicio = this.getIdServicioEntidad(bean.getIdEntidad());
		IClienteWS cliente = this.getCliente(idServicio);
		
		return cliente.solicitarAsistencia(bean.getCuil(), bean.getGeolocalizacion(), bean.getMensaje(), bean.getUrlAdjuntos());
	}
	
	public void enviarMensaje(MensajeBean bean) throws Exception {
		int idServicio = this.getIdServicioEntidadDeAsistencia(bean.getIdAsistencia());
		IClienteWS cliente = this.getCliente(idServicio);		
		cliente.agregarMensajeUsuario(this.getIdAsistenciaEntidad(bean.getIdAsistencia()), bean.getMensaje());
	}
	
	public void enviarNuevosMensajes(int idAsistencia, List<String> mensajes) throws Exception {
		int idServicio = this.getIdServicioEntidadDeAsistencia(idAsistencia);
		IClienteWS cliente = this.getCliente(idServicio);		
		cliente.agregarMensajesUsuario(this.getIdAsistenciaEntidad(idAsistencia), mensajes);
	}
	
	private boolean isAsistenciaFinalizada(int id) throws SQLException {
		MSAsistenciasDao dao = new MSAsistenciasDao();
		int idEstado = dao.get(id).getIdEstado();
		// TODO borrar hardcoded id estado para cancelada
		dao.close();
		return idEstado == 3 || idEstado == 4;
	}
	
	public void actualizarAsistencias(String cuil) throws Exception {
		MSAsistenciasDao dao = new MSAsistenciasDao();
		int[] idAsistencias = dao.asistenciasEnCursoIds(cuil);
		dao.close();
		if(idAsistencias == null)
			return;
		for(int id : idAsistencias)
			this.getAsistenciaFinalizada(id);
	}
	
	// obtiene si entidad marco la asistencia como finalizada, y la actualiza en la BD
	public boolean getAsistenciaFinalizada(int idAsistencia) throws Exception {
		if(this.isAsistenciaFinalizada(idAsistencia))
			return true;
		int idServicio = this.getIdServicioEntidadDeAsistencia(idAsistencia);
		IClienteWS cliente = this.getCliente(idServicio);

		boolean finalizada = cliente.isAsistenciaFinalizada(this.getIdAsistenciaEntidad(idAsistencia)); 
		if(finalizada) {
			MSAsistenciasDao dao = new MSAsistenciasDao();
			dao.finalizarAsistencia(idAsistencia);
			dao.close();
		}
		
		return finalizada;
	}
	
	public void actualizarNoticias() throws SQLException, Exception {
		ClienteSOAP cliente = new ClienteSOAP(this.obtenerCredenciales(5));
		List<NoticiaBean> noticias = cliente.obtenerNoticias();
		
		Dao<Void, NoticiaBean> dao = DaoFactory.getDao("Noticias", "ar.edu.ubp.das");
		for(NoticiaBean n: noticias) {
			n.setFuente("Municipalidad");
			dao.insert(n);
		}
	}
	
	public void enviarReportes() throws Exception {
		String publicPath = "/home/al/das/public";
		String endpointAdjuntos = "http://localhost:8080/GobiernoAPI/api/public/reportes";
		Dao<String, String> dao = DaoFactory.getDao("Reportes", "ar.edu.ubp.das");
		List<String> reporte = dao.select(null);
		
		String strDate = (new SimpleDateFormat("yyyy-MM-dd")).format(Calendar.getInstance().getTime());
		String filename = strDate+ ".csv";
		String publicFile = endpointAdjuntos +"/"+ filename;
		
		FileWriter file = new FileWriter(publicPath + "/" + filename);
		for(String r: reporte) {
			file.write(r);
		}
		file.close();
		
		ClienteSOAP cliente = new ClienteSOAP(this.obtenerCredenciales(5));
		cliente.enviarReportes(strDate, publicFile);
	}
}
