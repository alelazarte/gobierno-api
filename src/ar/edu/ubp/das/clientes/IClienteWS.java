package ar.edu.ubp.das.clientes;

import java.util.List;

import ar.edu.ubp.das.beans.SesionServicioBean;
import ar.edu.ubp.das.beans.MensajeBean;

public interface IClienteWS {
	public int solicitarAsistencia(String cuil, String geolocalizacion, String mensaje, String urlAdjuntos) throws Exception;
	public boolean cancelarAsistencia(int idAsistencia) throws Exception;
	public boolean isAsistenciaFinalizada(int id) throws Exception;
	public void obtenerToken() throws Exception;
	public void agregarMensajeUsuario(int asistenciaId, String mensaje) throws Exception;
	public void agregarMensajesUsuario(int asistenciaId, List<String> mensajes) throws Exception;
	public List<MensajeBean> nuevosMensajesEntidad(int idAsistencia) throws Exception;
}
