package ar.edu.ubp.das.clientes;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.beans.NoticiaBean;
import ar.edu.ubp.das.beans.SesionServicioBean;

public class ClienteSOAP implements IClienteWS {
	
	private SesionServicioBean credenciales;
	private Client cliente;

	public ClienteSOAP(SesionServicioBean credenciales) throws Exception {
		this.credenciales = credenciales;
		JaxWsDynamicClientFactory jdcf = JaxWsDynamicClientFactory.newInstance();
		this.cliente = jdcf.createClient(this.credenciales.getBaseUrl());
	
		this.obtenerToken();
		// http://localhost:8082/DefensaCivilCXF/services/DefensaCivilWSPort?wsdl
	}
	
	@Override
	public int solicitarAsistencia(String cuil, String geolocalizacion, String mensaje, String urlAdjuntos) throws Exception {
		// TODO Auto-generated method stub
		int id = 0;
		for(int r=0; r < 3; r++) {
			id = (int) this.cliente.invoke("solicitarAsistencia", this.credenciales.getUsuario(), this.credenciales.getToken(), cuil, geolocalizacion, mensaje, urlAdjuntos)[0];
			if(id > 0)
				break;
			this.obtenerToken();
		}
		return id;
	}

	@Override
	public boolean cancelarAsistencia(int id) throws Exception {
		// TODO Auto-generated method stub
		this.cliente.invoke("cancelarAsistencia", this.credenciales.getUsuario(), this.credenciales.getToken(), id);
		// TODO: actualizar servicio SOAP para responder boolean al cancelar
		return true;
	}

	@Override
	public boolean isAsistenciaFinalizada(int id) throws Exception {
		// TODO Auto-generated method stub
		boolean response = false;
		int reintentos = 0;
		
		do {
			try {
				response = (boolean) this.cliente.invoke("isAsistenciaFinalizada", this.credenciales.getUsuario(), this.credenciales.getToken(), id)[0];
				break;
			}
			catch(Exception e) {
				System.out.print("reintentando obtener token");
				this.obtenerToken();
				reintentos++;
			}
		} while(reintentos < 3);
		
		return response;
	}

	@Override
	public void obtenerToken() throws Exception {
		// TODO Auto-generated method stub
		String token = (String) this.cliente.invoke("obtenerToken", this.credenciales.getUsuario(), this.credenciales.getClave())[0];
		this.credenciales.setToken(token);
	}

	@Override
	public void agregarMensajeUsuario(int asistenciaId, String mensaje) throws Exception {
		// TODO Auto-generated method stub
		this.cliente.invoke("agregarMensajeUsuario", this.credenciales.getUsuario(), this.credenciales.getToken(), asistenciaId, mensaje);
	}

	@Override
	public void agregarMensajesUsuario(int asistenciaId, List<String> mensajes) throws Exception {
		// TODO Auto-generated method stub
		int reintentos = 0;
		do {
			try {
				this.cliente.invoke("agregarMensajesUsuario", this.credenciales.getUsuario(), this.credenciales.getToken(), asistenciaId, mensajes);
				break;
			}
			catch(Exception e) {
				System.out.print("reintentando obtener token");
				this.obtenerToken();
				reintentos++;
			}
		} while(reintentos < 3);
	}
	
	@Override
	public List<MensajeBean> nuevosMensajesEntidad(int asistenciaId) throws Exception {
		// TODO Auto-generated method stub
		// TODO: sacar el `int desde` porque no es necesario
		
		int reintentos = 0;
		Object[] response = null;
		
		do {
			try {
				response = this.cliente.invoke("nuevosMensajesEntidad", this.credenciales.getUsuario(), this.credenciales.getToken(), asistenciaId, 0);
				if(response.length > 0)
					break;
			}
			catch(Exception e) {
				System.out.print("reintentando obtener token");
				this.obtenerToken();
				reintentos++;
			}
		} while(reintentos < 3);

		if(response == null)
			return null;
		
		System.out.println("token actual: "+this.credenciales.getToken());
		
		ArrayList<String> mensajes = (ArrayList<String>) response[0];
		List<MensajeBean> mensajesBean = new ArrayList<MensajeBean>();
		mensajes.forEach(m -> {
			MensajeBean mensaje = new MensajeBean();
			mensaje.setMensaje(m);
			mensajesBean.add(mensaje);
		});
		return mensajesBean;
	}
	
	public List<NoticiaBean> obtenerNoticias() throws Exception {
		Object[] response = this.cliente.invoke("obtenerNoticias", this.credenciales.getUsuario(), this.credenciales.getToken());
		if(response.length <= 0)
			return null;
		List<String> listaNoticias = (ArrayList<String>) response[0];
		List<NoticiaBean> noticias = new ArrayList<NoticiaBean>();
		NoticiaBean bean;
		String[] partesNoticia = new String[3];
		for(String l: listaNoticias) {
			partesNoticia = l.split(":::");
			bean = new NoticiaBean();
			
			bean.setTitulo(partesNoticia[0]);
			bean.setAutor(partesNoticia[1]);
			bean.setCuerpo(partesNoticia[2]);
			
			noticias.add(bean);
		}
		return noticias;
	}
	
	public void enviarReportes(String periodo, String urlReportes) throws Exception {
		this.cliente.invoke("enviarReportes", this.credenciales.getUsuario(), this.credenciales.getToken(), periodo, urlReportes);
	}
}
