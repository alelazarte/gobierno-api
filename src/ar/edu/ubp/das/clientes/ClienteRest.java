package ar.edu.ubp.das.clientes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ar.edu.ubp.das.beans.SesionServicioBean;
import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.beans.MensajeBean;

public class ClienteRest implements IClienteWS {

	private SesionServicioBean credenciales;
	
	private Response consumir(String operacion, String tipoCredencial, String credencial, Object cuerpo) throws Exception {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(this.credenciales.getBaseUrl()+"/"+operacion);
		Response response;
		int reintentos = 0;
		do {
			System.out.println("reintento nro: "+reintentos);
			response = target.request(MediaType.APPLICATION_JSON)
					.header("usuario", this.credenciales.getUsuario())
					.header(tipoCredencial, credencial)
					.post(Entity.entity(cuerpo, MediaType.APPLICATION_JSON));
			
			if(response.getStatus() == Response.Status.OK.getStatusCode())
				break;
	
			this.credenciales.setToken(null);
			this.obtenerToken();
			// TODO: esto lo necesito hacer porque le mando la credencial como parametro, tengo 
			// que repensar eso
			credencial = this.credenciales.getToken();
			response.close();
			reintentos++;
			// TODO borrar esto
			Thread.sleep(1000);
		} while(reintentos <= 3);
		
		return response;
	}
	
	public ClienteRest(SesionServicioBean credenciales) throws Exception {
		this.credenciales = credenciales;
		this.obtenerToken();
	}

	@Override
	public int solicitarAsistencia(String cuil, String geolocalizacion, String mensaje, String urlAdjuntos) throws Exception {
		// TODO Auto-generated method stub
		AsistenciaBean bean = new AsistenciaBean();
		bean.setCuil(cuil);
		bean.setGeolocalizacion(geolocalizacion);
		bean.setMensaje(mensaje);
		bean.setUrlAdjuntos(urlAdjuntos);

		Response response = this.consumir(
				"solicitarAsistencia", 
				"token",
				this.credenciales.getToken(), 
				bean);
		
		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Solicitar asistencia fallo, status code: "+response.getStatus());
		
		return response.readEntity(AsistenciaBean.class).getId();
	}

	@Override
	public boolean cancelarAsistencia(int idAsistencia) throws Exception {
		// TODO Auto-generated method stub

		Response response = this.consumir(
				"cancelarAsistencia",
				"token",
				this.credenciales.getToken(), 
				new AsistenciaBean(idAsistencia));

		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Cancelar asistencia fallo, status code: "+response.getStatus());

		return true;
	}

	@Override
	public boolean isAsistenciaFinalizada(int id) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("isAsistenciaFinalizada("+id+")");
		Response response = this.consumir(
				"isAsistenciaFinalizada", 
				"token",
				this.credenciales.getToken(), 
				new AsistenciaBean(id));
		
		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Solicitar asistencia fallo, status code: "+response.getStatus());

		return response.readEntity(AsistenciaBean.class).isFinalizada();
	}

	@Override
	public void obtenerToken() throws Exception {
		// TODO Auto-generated method stub

		Response response = this.consumir(
				"obtenerToken", 
				"clave",
				this.credenciales.getClave(), 
				null);

		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Obtener token fallo, status code: "+response.getStatus());

		this.credenciales.setToken(response.readEntity(SesionServicioBean.class).getToken());
	}

	@Override
	public void agregarMensajeUsuario(int idAsistencia, String mensaje) throws Exception {
		// TODO Auto-generated method stub
		MensajeBean bean = new MensajeBean();
		bean.setIdAsistencia(idAsistencia);
		bean.setMensaje(mensaje);
		List<MensajeBean> mensajes = new ArrayList<MensajeBean>();
		mensajes.add(bean);

		Response response = this.consumir(
				"agregarMensajeUsuario", 
				"token",
				this.credenciales.getToken(), 
				mensajes);
		
		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Enviar mensaje fallo, status code: "+response.getStatus());		
	}

	@Override
	public void agregarMensajesUsuario(int idAsistencia, List<String> mensajes) throws Exception {
		// TODO Auto-generated method stub
		List<MensajeBean> cuerpo = new ArrayList<MensajeBean>();
		for(String m: mensajes) {
			MensajeBean bean = new MensajeBean();
			bean.setIdAsistencia(idAsistencia);
			bean.setMensaje(m);
			cuerpo.add(bean);
		}
		
		Response response = this.consumir(
				"agregarMensajeUsuario", 
				"token",
				this.credenciales.getToken(), 
				cuerpo);
		
		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Solicitar asistencia fallo, status code: "+response.getStatus());		
	}
	
	@Override
	public List<MensajeBean> nuevosMensajesEntidad(int idAsistencia) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("token rest("+this.credenciales.getIdServicio()+"): " + this.credenciales.getToken());
		List<MensajeBean> mensaje = new ArrayList<MensajeBean>();
		mensaje.add(new MensajeBean(idAsistencia));

		Response response = this.consumir(
				"nuevosMensajesEntidad",
				"token",
				this.credenciales.getToken(),
				mensaje);
		if(response == null)
			throw new Exception("Nuevos mensajes de entidad fallo");
		if(response.getStatus() != Response.Status.OK.getStatusCode())
			throw new Exception("Nuevos mensajes de entidad fallo, code: "+response.getStatus());
		
		List<MensajeBean> mensajes = new ArrayList<MensajeBean>();
		Iterator<HashMap> i = response.readEntity(List.class).iterator();
		MensajeBean msje;
		HashMap map;
		while(i.hasNext()) {
			map = i.next();
			System.out.println("nuevo mensaje para asistencia id "+idAsistencia+": "+map.get("mensaje"));
			mensajes.add(new MensajeBean((String) map.get("mensaje")));
		}
		
		return mensajes;
	}
	
}
