package ar.edu.ubp.das.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class NoticiaBean {
	String titulo, autor, cuerpo, fuente;
	Timestamp fechaPublicacion;
	
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	public LocalDateTime getFechaPublicacion() {
		return fechaPublicacion.toLocalDateTime();
	}

	public void setFechaPublicacion(Timestamp fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
}
