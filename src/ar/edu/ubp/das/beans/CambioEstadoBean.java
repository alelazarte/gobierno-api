package ar.edu.ubp.das.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CambioEstadoBean {
	String nombreEstado;
	int idAsistencia;
	Timestamp fechaCambio;

	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	public int getIdAsistencia() {
		return idAsistencia;
	}
	public void setIdAsistencia(int idAsistencia) {
		this.idAsistencia = idAsistencia;
	}
	public LocalDateTime getFechaCambio() {
		return fechaCambio.toLocalDateTime();
	}
	public void setFechaCambio(Timestamp fechaCambio) {
		this.fechaCambio = fechaCambio;
	}
}
