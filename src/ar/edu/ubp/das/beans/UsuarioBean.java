package ar.edu.ubp.das.beans;

import java.sql.Time;

import ar.edu.ubp.das.db.Bean;

public class UsuarioBean implements Bean {
    private String cuil, nombre, apellido, email, clave, tokenSesion;
	private boolean habilitado, verificado;
	
	Time tokenVencimiento;

    
	public Time getTokenVencimiento() {
		return tokenVencimiento;
	}

    public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void setTokenVencimiento(Time tokenVencimiento) {
		this.tokenVencimiento = tokenVencimiento;
	}

	public String getTokenSesion() {
		return tokenSesion;
	}

	public void setTokenSesion(String tokenSesion) {
		this.tokenSesion = tokenSesion;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public boolean isVerificado() {
		return verificado;
	}

	public void setVerificado(boolean verificado) {
		this.verificado = verificado;
	}



}

