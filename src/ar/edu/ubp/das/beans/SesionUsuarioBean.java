package ar.edu.ubp.das.beans;

import java.sql.Time;

public class SesionUsuarioBean {
	String cuil, tokenSesion;
	Time tokenVencimiento;

	public String getCuil() {
		return cuil;
	}
	public void setCuil(String cuil) {
		this.cuil = cuil;
	}
	public String getTokenSesion() {
		return tokenSesion;
	}
	public void setTokenSesion(String tokenSesion) {
		this.tokenSesion = tokenSesion;
	}
	public Time getTokenVencimiento() {
		return tokenVencimiento;
	}
	public void setTokenVencimiento(Time tokenVencimiento) {
		this.tokenVencimiento = tokenVencimiento;
	}
}
