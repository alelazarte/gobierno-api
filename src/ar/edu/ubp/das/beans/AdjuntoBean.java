package ar.edu.ubp.das.beans;

public class AdjuntoBean {
	int id, idAsistencia;
	String pathPublico;
	String nombreArchivo;
	

	public AdjuntoBean() { }
	
	public AdjuntoBean(String pathPublico, String nombreArchivo) {
		this.pathPublico = pathPublico;
		this.nombreArchivo = nombreArchivo;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAsistencia() {
		return idAsistencia;
	}
	public void setIdAsistencia(int idAsistencia) {
		this.idAsistencia = idAsistencia;
	}
	public String getPathPublico() {
		return pathPublico;
	}
	public void setPathPublico(String pathPublico) {
		this.pathPublico = pathPublico;
	}
}
