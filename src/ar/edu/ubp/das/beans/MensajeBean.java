package ar.edu.ubp.das.beans;

import java.sql.Date;

public class MensajeBean {
	int id, idAsistencia;
	String mensaje, cliente;
	Date fecha;
	boolean nuevos;

	public MensajeBean() {}
	public MensajeBean(int idAsistencia) {
		this.idAsistencia = idAsistencia;
	}
	public MensajeBean(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAsistencia() {
		return idAsistencia;
	}
	public void setIdAsistencia(int idAsistencia) {
		this.idAsistencia = idAsistencia;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public boolean isNuevos() {
		return nuevos;
	}
	public void setNuevos(boolean nuevos) {
		this.nuevos = nuevos;
	}
}
