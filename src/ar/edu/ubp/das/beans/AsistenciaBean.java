package ar.edu.ubp.das.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class AsistenciaBean {
	int id, idEntidad, idEstado, idAsisEntidad;
	String cuil, geolocalizacion, mensaje, nombreEntidad, nombreEstado, nombreUsuario, urlAdjuntos;
	Timestamp fechaSolicitada;
	boolean isFinalizada, isCancelada, chatHabilitado;
	ArchivoBean[] archivos;
	
	public AsistenciaBean() {}
	
	public AsistenciaBean(int id) {
		this.id = id;
	}
	
	
	public String getUrlAdjuntos() {
		return urlAdjuntos;
	}

	public void setUrlAdjuntos(String urlAdjuntos) {
		this.urlAdjuntos = urlAdjuntos;
	}

	public ArchivoBean[] getArchivos() {
		return archivos;
	}
	public void setArchivos(ArchivoBean[] archivos) {
		this.archivos = archivos;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public boolean isFinalizada() {
		return isFinalizada;
	}
	public void setFinalizada(boolean isFinalizada) {
		this.isFinalizada = isFinalizada;
	}
	public boolean isCancelada() {
		return isCancelada;
	}
	public void setCancelada(boolean isCancelada) {
		this.isCancelada = isCancelada;
	}
	public String getNombreEntidad() {
		return nombreEntidad;
	}
	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdEntidad() {
		return idEntidad;
	}
	public void setIdEntidad(int idEntidad) {
		this.idEntidad = idEntidad;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	public String getCuil() {
		return cuil;
	}
	public void setCuil(String cuil) {
		this.cuil = cuil;
	}
	public int getIdAsisEntidad() {
		return idAsisEntidad;
	}
	public void setIdAsisEntidad(int idAsisEntidad) {
		this.idAsisEntidad = idAsisEntidad;
	}
	public String getGeolocalizacion() {
		return geolocalizacion;
	}
	public void setGeolocalizacion(String geolocalizacion) {
		this.geolocalizacion = geolocalizacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public LocalDateTime getFechaSolicitada() {
		if(fechaSolicitada == null)
			return null;
		return fechaSolicitada.toLocalDateTime();
	}
	public void setFechaSolicitada(Timestamp fechaSolicitada) {
		this.fechaSolicitada = fechaSolicitada;
	}
	public boolean isChatHabilitado() {
		return chatHabilitado;
	}
	public void setChatHabilitado(boolean chatHabilitado) {
		this.chatHabilitado = chatHabilitado;
	}
	
	
}
