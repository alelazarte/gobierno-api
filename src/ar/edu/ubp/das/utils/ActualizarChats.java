package ar.edu.ubp.das.utils;

import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.clientes.ClienteEntidad;
import ar.edu.ubp.das.daos.MSAsistenciasDao;
import ar.edu.ubp.das.daos.MSChatsDao;

public class ActualizarChats {

	public static void main(String[] args) throws SQLException, InterruptedException {
		ClienteEntidad cliente = new ClienteEntidad();
		MSAsistenciasDao asistenciasDao = new MSAsistenciasDao();
		MSChatsDao chatsDao = new MSChatsDao();

		while(true) {
			// get todas asistencias en curso para cualquier CUIL
			int[] ids = asistenciasDao.asistenciasEnCursoIds("*");
			System.out.println("nro de asistencias en curso: "+ids.length);
			try {
				for(int i=0 ; i<ids.length ; i++) {
					System.out.println("trabajando en asistencia id: "+ids[i]);
					cliente.cargarNuevosMensajes(ids[i]);
					List<String> mensajesUsuario = chatsDao.nuevosMensajesUsuario(ids[i]);
					if(mensajesUsuario != null && mensajesUsuario.size() > 0) {
						cliente.enviarNuevosMensajes(ids[i], mensajesUsuario);	
					}
		    		cliente.getAsistenciaFinalizada(ids[i]);
		    		// sleep per request just in case
		    		Thread.sleep(3000);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				asistenciasDao.close();
				chatsDao.close();
			}
			// sleep too after checking assistances
			Thread.sleep(3000);
		}
	}

}
