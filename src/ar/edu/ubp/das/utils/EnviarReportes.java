package ar.edu.ubp.das.utils;

import java.sql.SQLException;

import ar.edu.ubp.das.clientes.ClienteEntidad;

public class EnviarReportes {
	public static void main(String[] args) throws SQLException, InterruptedException {
		ClienteEntidad cliente = new ClienteEntidad();
		try {
			cliente.enviarReportes();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
